create table users
(
  id int unsigned auto_increment,
  name varchar(100) null,
  email varchar(100) null comment 'user unique identifier in the system',
  is_active int null comment 'can be only 0 - false or 1 - true, default true',
  created_at datetime null,
  primary key (`id`),
  index (`created_at`)
);

create table posts
(
  id int unsigned auto_increment,
  user_id int signed null,
  title text null,
  body longtext null,
  created_at datetime null,
  updated_at datetime null,
  primary key (`id`),
  index (`user_id`),
  index (`created_at`)
);