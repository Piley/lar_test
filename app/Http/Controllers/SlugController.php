<?php

namespace App\Http\Controllers;

use App\Http\Requests\SlugRequest;
use App\Slug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class SlugController extends Controller
{

    /**
     * Store a newly created resource in storage.
     */
    public function genarate(SlugRequest $request)
    {
        $slug = new Slug();
        $slug->fill($request->all());
        $slug->slug = Slug::generateShort($slug->url);
        $slug->save();
        return response()->json(
            ['short_url' => URL::to('/') . "/" . $slug->slug], 201);
    }

}
