<?php

namespace App\Http\Controllers;

use App\Slug;
use App\Stat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MainController extends Controller
{
    /**
     * Redirect to real url.
     */
    public function index($url)
    {
        $slug = Slug::where('slug', $url)->firstOrFail();
        Stat::create(['slug_id' => $slug->id]);
        return Redirect::to($slug->url);
    }
}
