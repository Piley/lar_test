<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Slug extends Model
{
    protected $fillable = ['url'];

    public static function generateShort($url)
    {
        $id = DB::table('slugs')->max('id') + 1;
        return base_convert($id, 10, 36);
    }
}
